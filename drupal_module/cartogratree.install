<?php

/**
 * @file
 * CartograTree installation
 */

/**
 * Implements hook_requirements().
 * Check installation requirements (i.e., OpenLayers3 files are in 'sites/all/libraries/openlayers'.
 */
function cartogratree_requirements($phase) {
    $requirements = array();
    // Ensure translations don't break at install time
    $t = get_t();

    // Check to see if the OpenLayers library is available for CartograTree
    if ($phase == 'runtime') {
        $library = libraries_detect('openlayers');

        if ($library['installed']) {
            $version = explode('.', $library['version']);

            if ($version[0] == CARTOGRATREE_COMPATIBLE_MAJOR_VERSION) {
                $requirements['cartogratree'] = array(
                  'value' => $library['version'],
                  'severity' => REQUIREMENT_OK,
                );
            }
            else {
                $requirements['cartogratree'] = array(
                  'value' => $library['version'],
                  'description' => $t('Incompatible version detected. The OpenLayers library version for CartograTree must be from the %version.x branch.', array('%version' => CARTOGRATREE_COMPATIBLE_MAJOR_VERSION)),
                  'severity' => REQUIREMENT_WARNING,
                );
            }
        }
        else {
            $requirements['cartogratree'] = array(
              'value' => $t('OpenLayers library for CartograTree not found.'),
              'description' => $t('The OpenLayers library for CartograTree could not be detected. Please consult the README.md for installation instructions.'),
              'severity' => REQUIREMENT_ERROR,
            );
        }

        $requirements['cartogratree']['title'] = $t('CartograTree');
    }

    return $requirements;
}

/**
 * Implements hook_schema().
 * Define the two tables used by CartograTree, cartogratree_layers and cartogratree_layer_permissions.
 */
function cartogratree_schema() {
    $schema = array();

    $schema['cartogratree_layers'] = array(
      'description' => 'Keeps track of layers for use with CartograTree module.',
      'fields' => array(
        'layer_id' => array(
          'description' => 'Primary Key',
          'type' => 'serial',
          'not null' => TRUE,
        ),
        'title' => array(
          'description' => 'Human-readable name for the layer.',
          'type' => 'varchar',
          'length' => 512,
        ),
        'name' => array(
          'description' => 'Machine name for the layer.',
          'type' => 'varchar',
          'length' => 256,
        ),
        'url' => array(
          'description' => 'The URL for the provider of the layer.',
          'type' => 'varchar',
          'length' => 256,
        ),
        'group_id' => array(
          'description' => 'Accordion group ID for side navigation menu.',
          'type' => 'int',
        ),
        'subgroup_id' => array(
          'description' => 'Accordion subgroup ID for side navigation menu.',
          'type' => 'int',
        ),
        'layer_rank' => array(
          'description' => 'Layer rank for side navigation menu.',
          'type' => 'int',
        ),
        'trees_layer' => array(
          'description' => 'Indicates whether this layer has trees (1/yes) or other/environmental data (0/no).',
          'type' => 'int',
        ),
        'layer_type' => array(
          'description' => 'Indicates the type of layer: vector, or raster.',
          'type' => 'character',
          'length' => 6,
          'not null' => FALSE,
        ),
      ),
      'primary key' => array('layer_id'),
    );

    $schema['cartogratree_layer_permissions'] = array(
      'description' => 'Keeps track of permissions for layers.',
      'fields' => array(
        'layer_permissions_id' => array(
          'description' => 'Primary Key',
          'type' => 'serial',
          'not null' => TRUE,
        ),
        'layer_id' => array(
          'description' => 'Links to the layers table.',
          'type' => 'int',
          'not null' => TRUE,
        ),
        'rid' => array(
          'description' => 'The ROLE ID of a role to provide access to.',
          'type' => 'int',
        ),
        'uid' => array(
          'description' => 'The USER ID of a user to provide access to.',
          'type' => 'int',
        ),
      ),
      'primary key' => array('layer_permissions_id'),
    );

    $schema['cartogratree_groups'] = array(
      'description' => 'Keeps track of accordion groups for side navigation menu.',
      'fields' => array(
        'group_id' => array(
          'description' => 'Primary Key',
          'type' => 'serial',
          'not null' => TRUE,
        ),
        'group_name' => array(
          'description' => 'Accordion group name for side navigation menu.',
          'type' => 'varchar',
          'length' => 256,
        ),
        'group_rank' => array(
          'description' => 'Group rank for side navigation menu.',
          'type' => 'int',
        ),
      ),
      'primary key' => array('group_id'),
    );

    $schema['cartogratree_subgroups'] = array(
      'description' => 'Keeps track of accordion subgroups for side navigation menu.',
      'fields' => array(
        'subgroup_id' => array(
          'description' => 'Primary Key',
          'type' => 'serial',
          'not null' => TRUE,
        ),
        'subgroup_name' => array(
          'description' => 'Accordion subgroup name for side navigation menu.',
          'type' => 'varchar',
          'length' => 256,
        ),
      ),
      'primary key' => array('subgroup_id'),
    );

    $schema['cartogratree_groups_subgroups'] = array(
      'description' => 'Keeps track of accordion hierarchy for side navigation menu.',
      'fields' => array(
        'group_id' => array(
          'description' => 'FK from cartogratree_groups',
          'type' => 'int',
          'not null' => TRUE,
        ),
        'subgroup_id' => array(
          'description' => 'FK from cartogratree_subgroups',
          'type' => 'int',
          'not null' => TRUE,
        ),
        'subgroup_rank' => array(
          'description' => 'Subgroup rank for side navigation menu.',
          'type' => 'int',
        ),
      ),
      'primary key' => array('group_id', 'subgroup_id', 'subgroup_rank'),
    );

    $schema['cartogratree_fields'] = array(
      'description' => 'Keeps track of fields to filter by, display in pop-up, and display in table for each layer.',
      'fields' => array(
        'field_id' => array(
          'description' => 'Primary Key',
          'type' => 'serial',
          'not null' => TRUE,
        ),
        'layer_id' => array(
          'description' => 'Parent layer of this field.', // FK from cartogratree_layers
          'type' => 'int',
        ),
        'field_name' => array(
          'description' => 'Name returned by layer.',
          'type' => 'varchar',
          'length' => 256,
        ),
        'display_name' => array(
          'description' => 'Name shown to user.',
          'type' => 'varchar',
          'length' => 256,
        ),
        'filter' => array(
          'description' => 'Allow users to filter data by this field (1), or not (0).',
          'type' => 'int',
          'default' => 0,
        ),
        'filter_type' => array(
          'description' => 'Type of filter to use with this field: radio-button, checkbox, slider, or elastic search (radio, checkbox, slider, or elastic, respectively).',
          'type' => 'varchar',
          'length' => 10,
        ),
        'field_values' => array(
          'description' => 'Range for slider (e.g., 0..20), and semi-colon sperated list for radio and check.',
          'type' => 'text',
        ),
        'precision' => array(
          'description' => 'Precision used with range values.',
          'type' => 'int',
          'default' => 2,
        ),
        'mask_value' => array(
          'description' => 'Value returned by layer that should be masked.',
          'type' => 'varchar',
          'length' => 256,
        ),
        'mask_display' => array(
          'description' => 'Text shown to user for masked values.',
          'type' => 'varchar',
          'length' => 256,
        ),
        'pop_up' => array(
          'description' => 'Show this field in maps pop-up (1), or not (0).',
          'type' => 'int',
          'default' => 0,
        ),
        'data_table' => array(
          'description' => 'Show this field in data table (1), or not (0).',
          'type' => 'int',
          'default' => 0,
        ),
        'field_rank' => array(
          'description' => 'Order in which this field is shown.',
          'type' => 'int',
        ),
      ),
      'primary key' => array('field_id'),
    );

    $schema['cartogratree_environmental_values'] = array(
      'description' => 'Stores environmental values for geo-referenced trees in the database.',
      'fields' => array(
        'latitude' => array(
          'description' => 'Latitude of the location, in WGS84 projection.',
          'type' => 'varchar',
          'length' => 20,
        ),
        'longitude' => array(
          'description' => 'Longitude of the location, in WGS84 projection.',
          'type' => 'varchar',
          'length' => 20,
        ),
        'layer_id' => array(
          'description' => 'ID of the environmental layer.',
          'type' => 'int',
        ),
        'field_id' => array(
          'description' => 'ID of the field in the environmental layer.',
          'type' => 'int',
        ),
        'field_name' => array(
          'description' => 'Text shown to user for the field in the environmental layer.',
          'type' => 'varchar',
          'length' => 64,
        ),
        'field_value' => array(
          'description' => 'Value of the field in the environmental layer.',
          'type' => 'varchar',
          'length' => 8096,
        ),
      ),
      'primary key' => array('latitude', 'longitude', 'layer_id', 'field_id'),
    );

    $schema['cartogratree_sessions'] = array(
      'description' => 'Keeps track of sessions.',
      'fields' => array(
        'id' => array(
          'description' => 'Primary Key',
          'type' => 'serial',
          'not null' => TRUE,
        ),
        'session_id' => array(
          'description' => 'Session identifier.',
          'type' => 'char',
          'length' => 32,
        ),
        'operation_type' => array(
          'description' => 'Type of operation for this session_id: 0/insert, 1/read, or 2/write.',
          'type' => 'int',
        ),
        'operation_timestamp' => array(
          'description' => 'Time when operation was performed for this session_id.',
          'type' => 'varchar',
          'length' => 20,
        ),
        'remote_addr' => array(
          'description' => 'IP address of the client.',
          'type' => 'varchar',
          'length' => 16,
        ),
        'http_user_agent' => array(
          'description' => 'Browser details of the client.',
          'type' => 'varchar',
          'length' => 1024,
        ),
        'uid' => array(
          'description' => 'Drupal user ID.',
          'type' => 'int',
        ),
        'rids' => array(
          'description' => 'Drupal role IDs.',
          'type' => 'varchar',
          'length' => 10,
        ),
      ),
      'primary key' => array('id'),
    );

    $schema['cartogratree_session_layers'] = array(
      'description' => 'Keeps track of layers used in each session.',
      'fields' => array(
        'id' => array(
          'description' => 'Primary Key of cartogratree_sessions.',
          'type' => 'int',
          'not null' => TRUE,
        ),
        'layer_id' => array(
          'description' => 'Layer ID.',
          'type' => 'int',
          'not null' => TRUE,
        ),
      ),
      'primary key' => array('id', 'layer_id'),
    );

    $schema['cartogratree_session_filters'] = array(
      'description' => 'Keeps track of filters used in each session.',
      'fields' => array(
        'id' => array(
          'description' => 'Primary Key of cartogratree_sessions.',
          'type' => 'int',
          'not null' => TRUE,
        ),
        'layer_id' => array(
          'description' => 'Layer ID.',
          'type' => 'int',
          'not null' => TRUE,
        ),
        'filter_id' => array(
          'description' => 'Filter ID.',
          'type' => 'int',
          'not null' => TRUE,
        ),
        'field_value' => array(
          'description' => 'Value selected for this field.',
          'type' => 'varchar',
          'length' => 128,
        ),
      ),
      'primary key' => array('id', 'layer_id', 'filter_id'),
    );

    $schema['cartogratree_session_records'] = array(
      'description' => 'Keeps track of individuals selected in each session.',
      'fields' => array(
        'id' => array(
          'description' => 'Primary Key of cartogratree_sessions.',
          'type' => 'int',
          'not null' => TRUE,
        ),
        'record_id' => array(
          'description' => 'ID of the individual/tree.',
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
        ),
      ),
      'primary key' => array('id', 'record_id'),
    );

    return $schema;
}

function cartogratree_update_7002() {
    db_add_field(
        'cartogratree_layers',
        'layer_type',
        array(
            'description' => 'Indicates the type of layer: vector, or raster.',
            'type' => 'char',
            'length' => 6,
            'not null' => FALSE,
        )
    );
}

function cartogratree_update_7003() {
    // drush updatedb
    // Drupal data types: https://www.drupal.org/node/159605
    // PostgreSQL data types: https://www.postgresql.org/docs/9.5/static/datatype.html
    db_create_table(
        'cartogratree_environmental_values',
        array(
        'description' => 'Stores environmental values for geo-referenced trees in the database.',
        'fields' => array(
          'latitude' => array(
            'description' => 'Latitude of the location, in WGS84 projection.',
            'type' => 'float',
          ),
          'longitude' => array(
            'description' => 'Longitude of the location, in WGS84 projection.',
            'type' => 'float',
          ),
          'layer_id' => array(
            'description' => 'ID of the environmental layer.',
            'type' => 'int',
          ),
          'field_id' => array(
            'description' => 'ID of the field in the environmental layer.',
            'type' => 'int',
          ),
          'field_name' => array(
            'description' => 'Text shown to user for the field in the environmental layer.',
            'type' => 'varchar',
            'length' => 64,
          ),
          'field_value' => array(
            'description' => 'Value of the field in the environmental layer.',
            'type' => 'varchar',
            'length' => 8096,
          ),
        ),
        'primary key' => array('latitude', 'longitude', 'layer_id', 'field_id'),
        )
    );
}

function cartogratree_update_7004() {
    db_drop_primary_key('cartogratree_environmental_values');
    db_change_field('cartogratree_environmental_values', 'latitude', 'latitude',
        array(
            'description' => 'Latitude of the location, in WGS84 projection.',
            'type' => 'varchar',
            'length' => 20,
        ));
    db_change_field('cartogratree_environmental_values', 'longitude', 'longitude',
        array(
            'description' => 'Longitude of the location, in WGS84 projection.',
            'type' => 'varchar',
            'length' => 20,
        ),
        array('primary key' => array('latitude', 'longitude', 'layer_id', 'field_id')));
}

function cartogratree_update_7005() {
    db_create_table(
        'cartogratree_sessions',
        array(
            'description' => 'Keeps track of sessions.',
            'fields' => array(
              'id' => array(
                'description' => 'Primary Key',
                'type' => 'serial',
                'not null' => TRUE,
              ),
              'session_id' => array(
                'description' => 'Session identifier.',
                'type' => 'char',
                'length' => 32,
              ),
              'operation_type' => array(
                'description' => 'Type of operation for this session_id: 0/insert, 1/read, or 2/write.',
                'type' => 'int',
              ),
              'operation_timestamp' => array(
                'description' => 'Time when operation was performed for this session_id.',
                'type' => 'varchar',
                'length' => 20,
              ),
              'remote_addr' => array(
                'description' => 'IP address of the client.',
                'type' => 'varchar',
                'length' => 16,
              ),
              'http_user_agent' => array(
                'description' => 'Browser details of the client.',
                'type' => 'varchar',
                'length' => 1024,
              ),
              'uid' => array(
                'description' => 'Drupal user ID.',
                'type' => 'int',
              ),
              'rid' => array(
                'description' => 'Drupal role ID.',
                'type' => 'int',
              ),
            ),
            'primary key' => array('id'),
        )
    );

    db_create_table(
        'cartogratree_session_layers',
        array(
            'description' => 'Keeps track of layers used in each session.',
            'fields' => array(
              'id' => array(
                'description' => 'Primary Key of cartogratree_sessions.',
                'type' => 'int',
                'not null' => TRUE,
              ),
              'layer_id' => array(
                'description' => 'Layer ID.',
                'type' => 'int',
                'not null' => TRUE,
              ),
            ),
            'primary key' => array('id', 'layer_id'),
        )
    );

    db_create_table(
        'cartogratree_session_filters',
        array(
            'description' => 'Keeps track of filters used in each session.',
            'fields' => array(
              'id' => array(
                'description' => 'Primary Key of cartogratree_sessions.',
                'type' => 'int',
                'not null' => TRUE,
              ),
              'layer_id' => array(
                'description' => 'Layer ID.',
                'type' => 'int',
                'not null' => TRUE,
              ),
              'filter_id' => array(
                'description' => 'Filter ID.',
                'type' => 'int',
                'not null' => TRUE,
              ),
              'field_value' => array(
                'description' => 'Value selected for this field.',
                'type' => 'varchar',
                'length' => 128,
              ),
            ),
            'primary key' => array('id', 'layer_id', 'filter_id'),
        )
    );

    db_create_table(
        'cartogratree_session_records',
        array(
            'description' => 'Keeps track of individuals selected in each session.',
            'fields' => array(
              'id' => array(
                'description' => 'Primary Key of cartogratree_sessions.',
                'type' => 'int',
                'not null' => TRUE,
              ),
              'record_id' => array(
                'description' => 'ID of the individual/tree.',
                'type' => 'int',
                'not null' => TRUE,
              ),
            ),
            'primary key' => array('id', 'record_id'),
        )
    );
}

function cartogratree_update_7006() {
    db_change_field('cartogratree_sessions', 'rid', 'rids',
        array(
            'description' => 'Drupal role IDs.',
            'type' => 'varchar',
            'length' => 10,
        ));
}

function cartogratree_update_7007() {
    db_change_field('cartogratree_session_records', 'record_id', 'record_id',
        array(
            'description' => 'ID of the individual/tree.',
            'type' => 'varchar',
            'length' => 128,
            'not null' => TRUE,
        ));
}
