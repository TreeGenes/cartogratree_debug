"use strict";
var mapState;
//var treeDataStore = {};
$(function () {
	console.log(Drupal.settings);
	//query builder set up
	var rulesBasic = {
		condition: "AND",
		rules: [{
			id: "family",
			operator: "equal",
		}, ]
	};

	var filtersList = [
		buildSelectOption("family", "Family", "string", ["equal", "not_equal"]),	
		buildSelectOption("genus", "Genus", "string", ["equal", "not_equal"]),
		buildSelectOption("species", "Species", "string", ["equal", "not_equal"]),
		buildSelectOption("marker_type", "Markers", "string", ["equal", "not_equal"]),
		buildSelectOption("structure_name", "Plant Structure", "string", ["equal", "not_equal"]),
		buildSelectOption("cvterm_name", "Phenotype Attribute", "string", ["equal", "not_equal"]),
		buildSelectOption("title", "Study Title", "string", ["equal", "not_equal"]),
		buildSelectOption("author", "Study Author", "string", ["equal", "not_equal"]),
		buildSelectOption("accession", "Study Accession", "string", ["equal", "not_equal"]),
	];

	function buildSelectOption(optionId, optionLabel, optionType, optionOps) {
		return {
			id: optionId,
			label: optionLabel,
			type: optionType,
			input: "select",
			values: {},
			operators: optionOps,
		}
	}
	
	populateOptions(0, Object.keys(Drupal.settings.options_data.organism_data["family"]).sort());
	populateOptions(1, Object.keys(Drupal.settings.options_data.organism_data["genus"]).sort());
	populateOptions(2, Drupal.settings.options_data.organism_data["species"].sort());
	populateOptions(3, Drupal.settings.options_data.marker_type);
	populateOptions(4, Drupal.settings.options_data.plant_ontology);
	populateOptions(5, Drupal.settings.options_data.cvterm);
	//populateOptions(5, Drupal.settings.options_data.phenotype);
	populateOptions(6, Drupal.settings.options_tgdr["pub_title"].sort());
	populateOptions(7, Drupal.settings.options_tgdr["pub_author"].sort());
	populateOptions(8, Drupal.settings.options_tgdr["pub_tgdr"].sort());

	function populateOptions(idx, optionsList) {
		var optionObj = filtersList[idx];
		for (var i = 0; i < optionsList.length; i++) {
			if (optionsList[i] != null && optionsList[i].length > 0) {
				optionObj["values"][optionsList[i]] = optionsList[i];
			}
		}
	}		

	$("#builder").queryBuilder({
		filters: filtersList,
		rules: rulesBasic,
		default_filter: 'family'
	});
	
	$("#body-row .collapse").collapse("hide");
	$("#collapse-icon").addClass("fa-angle-double-left");
	$("#map-options").collapse("show");	
	$("#map-summary").collapse("show");

	$("body").tooltip({
		selector: "[data-toggle='tooltip']",
		html: true,
	});

	$(".analysis-form-next").click(function() {
		console.log("go next");
		$(".nav-tabs > li > .active").parent().next("li").find("a").trigger("click");
		//$(".nav-tabs > .active").
	});

	$(".analysis-form-prev").click(function() {
		$(".nav-tabs > li > .active").parent().prev("li").find("a").trigger("click");
	});

	$(document).on("click", ".saved-session", function () {
		$(".saved-session.active").toggleClass("active");
		$(this).toggleClass("active");
	});

	//Change layout based on screen size
	modifyMapContainer();
	function modifyMapContainer() {
		if ($(window).width() <= 700) {
			$("#map-container").removeClass("col");
			$("#map-container").removeClass("py-3");
			$("#body-row").removeClass("row");
		}
		else {
			if (!$("#map-container").hasClass("col")) {
				$("#map-container").removeClass("col");
				$("#map-container").removeClass("py-3");
				$("#body-row").removeClass("row");
			}
		}
	}

	//change layout if screen size changes
	$(window).resize(function () {
		modifyMapContainer();
	});

	$(document).on("click", "#logout-btn", function () {
		if (confirm("Are you sure you want to logout?")) {
			window.open("/user/logout", "_blank");
			location.reload();
		}
	});
	
	$(document).on("click", ".map-state", function () {
		$(".map-state").removeClass("active").removeClass("bg-warning");
		if($(this).hasClass("active")){
			$(this).removeClass("active").removeClass("bg-warning");
		}
		else{
			$(this).addClass("active").addClass("bg-warning");
		}
	});

	$("#regenerate-api-key").on("click", function() {
		$.ajax({
			method: "GET",
			url: "https://tgwebdev.cam.uchc.edu/cartogratree/api/v2?user_id=" + Drupal.settings.user.user_id + "&new_key=true",
			contentType: "application/json", 
			success: function (data) {
				Drupal.settings.token = data;
				console.log(data);
				$("#api-key-holder").val(data);
				/*
				data = Object.values(data["snps_to_missing_freq"]));
				console.log(data);	
				renderChart(data, true);*/		
			},	
			error: function (xhr, textStatus, errorThrown) {
				console.log({
					textStatus
				});
				console.log({
					errorThrown
				});
				console.log(eval("(" + xhr.responseText + ")"));
			}
		});
	});

	$("#copy-api-key").on("click", function() {
		var copyText = document.getElementById("api-key-holder");
		copyText.select();
  		document.execCommand("copy");
	});

	$("#show-form-btn").on("click", function () {
		$("#save-query-title").val("");
		$("#save-query-comments").val("");
		$("#save-success-message").text("");
		if (!$("#save-success-container").hasClass("hidden")) {
			$("#save-success-container").addClass("hidden");
		}
		if ($("#submit-save-query").hasClass("hidden")) {
			$("#submit-save-query").removeClass("hidden");
		}
		if ($("#save-query-form").hasClass("hidden")) {
			$("#save-query-form").removeClass("hidden");
		}
		$("#save-form").modal("toggle");
	});

	$("#toggle-map-summary").on("click", function () {
		$("#map-summary").toggleClass("hidden");
	});


	$(".btn-toggle").on("click", function() {
		$(this).toggleClass("active");
	});	

	// -- Loading spinner when requesting data using ajax, will stop spinning once all ajax calls are done
	$(document).ajaxStop(function() {
		$(".loading-spinner").addClass("hidden");
		$(".dynamic-data").removeClass("hidden");
	});



});



function show_full_image(img) {
	$('#tree-full-picture-img').attr("src",img);
	$('#tree-full-picture').modal();
}

function show_filter_instructions() {
	$('#filter-instructions').modal();
}	
