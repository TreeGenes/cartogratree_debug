"use strict";
module.exports = function(app) {

	//old API deprecated functions
	var old_controller = require("./controllers/oldController");

	app.get("/cartogratree/api/v1.0/trees/markers/snps", old_controller.trees_snps);
	app.get("/cartogratree/api/v1.0/trees/markers/snps/all", old_controller.snps_all);
	app.get("/cartogratree/api/test", old_controller.show_me);
	
	//API V2 new end points and controllers
	var tree_controller = require("./controllers/treeController");
	var user_controller = require("./controllers/userController");  

	app.get("/cartogratree/api/v2/tree", tree_controller.tree_get_basic); 
	app.get("/cartogratree/api/v2/tree/treesnap", tree_controller.tree_get_treesnap);
	app.get("/cartogratree/api/v2/phenotypes", tree_controller.phenotypes_tree_get);
	app.get("/cartogratree/api/v2/genotypes", tree_controller.genotypes_tree_get);
	app.get("/cartogratree/api/v2/markertypes", tree_controller.markertypes_tree_get);
	app.get("/cartogratree/api/v2/publications", tree_controller.publications_tree_get);

	app.get("/cartogratree/api/v2/fields/trees", tree_controller.trees_get_fields);
	app.get("/cartogratree/api/v2/fields/markers", tree_controller.markers_get_fields);

	app.get("/cartogratree/api/v2/data/neon/summary",tree_controller.data_get_neon_site_summary);

	//These are administrative functions that can be executed via the API.
	//These endpoints require the ASUP (api_superuser_password) from config.js.
	//Simply append ?asup=password to the end of the url to perform the command.
	app.get("/cartogratree/api/v2/trees/remove_treegenes_trees", tree_controller.remove_treegenes_trees);
	app.get("/cartogratree/api/v2/trees/reload", tree_controller.trees_reload);
	app.get("/cartogratree/api/v2/trees/reload_status", tree_controller.trees_reload_status);
	app.get("/cartogratree/api/v2/trees/clear", tree_controller.cache_clear);
	app.get("/cartogratree/api/v2/trees/treesnap/consolidate_subkingdoms", tree_controller.treesnap_consolidate_subkingdoms);	
	app.get("/cartogratree/api/v2/trees/treesnap/consolidate_families", tree_controller.treesnap_consolidate_families);
	app.get("/cartogratree/api/v2/trees/treesnap/reload", tree_controller.reload_treesnap);
	app.get("/cartogratree/api/v2/trees/treesnap/force_reload", tree_controller.force_reload_treesnap);
	app.get("/cartogratree/api/v2/trees/dryad/consolidate_dryad_trees_from_treegenes", tree_controller.dryad_consolidate_trees_from_treegenes);
	app.get("/cartogratree/api/v2/trees/bienv4/trees/force_reload", tree_controller.force_reload_bienv4);
	app.get("/cartogratree/api/v2/trees/bienv4/trees/force_reload_linkedwithtganddryad", tree_controller.force_reload_bienv4_linkedwithtganddryad);
	app.get("/cartogratree/api/v2/trees/bienv4/trees/force_generategeometry_linkedwithtganddryad", tree_controller.force_generategeometry_bienv4_linkedwithtganddryad);
	app.get("/cartogratree/api/v2/trees/bienv4/trees/reload", tree_controller.reload_bienv4);
	app.get("/cartogratree/api/v2/trees/bienv4/trees/cleanup", tree_controller.cleanup_bienv4);
	app.get("/cartogratree/api/v2/trees/bienv4/endangered_taxa/force_reload", tree_controller.force_reload_bienv4_endangeredtaxa);
	

	app.get("/cartogratree/api/v2/trees", tree_controller.trees_list);
	app.get("/cartogratree/api/v2/trees/by_source_id", tree_controller.trees_list_by_source_id);
	app.post("/cartogratree/api/v2/trees/q", tree_controller.trees_list_qbuilder);
	app.get("/cartogratree/api/v2/trees/source", tree_controller.trees_by_source);
	app.get("/cartogratree/api/v2/trees/family", tree_controller.trees_by_family);
	app.get("/cartogratree/api/v2/trees/genus", tree_controller.trees_by_genus);
	app.get("/cartogratree/api/v2/trees/species", tree_controller.trees_by_species);
	app.get("/cartogratree/api/v2/trees/pub", tree_controller.trees_by_publication);
	app.get("/cartogratree/api/v2/trees/genotype", tree_controller.trees_by_genotype);
	app.get("/cartogratree/api/v2/trees/phenotype", tree_controller.trees_by_phenotype);
	app.get("/cartogratree/api/v2/trees/snp", tree_controller.trees_snp);
	app.post("/cartogratree/api/v2/trees/snp/missing", tree_controller.trees_snps_missing);

	app.get("/cartogratree/api/v2/stats/num/species", tree_controller.total_species);
	app.get("/cartogratree/api/v2/stats/num/pub", tree_controller.total_publications);
	app.get("/cartogratree/api/v2/stats/num/trees", tree_controller.total_trees);
	
	app.get("/cartogratree/api/v2", user_controller.get_api_key);

	app.post("/cartogratree/api/v2/user/session/save", user_controller.session_save);
	app.post("/cartogratree/api/v2/user/session/save/by-user", user_controller.session_save_user); 
	app.get("/cartogratree/api/v2/user/session", user_controller.session_get); 
	app.get("/cartogratree/api/v2/user/session/by-user", user_controller.session_get_user);
	app.get("/cartogratree/api/v2/user/session/by-user/all", user_controller.sessions_all_get_user);
	app.get("/cartogratree/api/v2/user/session/create", user_controller.session_create);
	app.get("/cartogratree/api/v2/user/session/delete", user_controller.session_delete);
};
