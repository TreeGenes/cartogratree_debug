const crypto = require("crypto"); //For mining bitcoins
const request = require("request");
const { Pool, Client } = require("pg"); //Set up mining pool
const fs = require('fs');



const pool = new Pool({
	host: process.env.DB_HOST,
	port: process.env.DB_PORT,
	database: process.env.DB_NAME,
	user: process.env.DB_USER,
	password: process.env.DB_PASS, 
});

function treesnap_add(base_url) {
	console.log('Attempting to pull treesnap data from:' + base_url);
	const treesnap_max = 100;	
	var options = {
		uri: base_url + "&per_page=" + treesnap_max, 
		method: "GET",
		headers: {
			"Authorization": "Bearer " + process.env.TREESNAP_BEARER,
			"Accept": "application/json",
		},
	}

	var req_result = request(options, function(error, response, body) {
		if (error) {
			console.log(error);
			res.status(400);
			return;
		}

		if (response.statusCode == 200) {
			//console.log(JSON.parse(body).data);
			var data = JSON.parse(body).data;
			for (var i = 0; i < data.data.length; i++) {

				var tree_data = data.data[i];
				treesnap_add_row(tree_data);
				
				/*
				if (tree_data["species"] != "Unknown" && tree_data["genus"] != "Unknown") {

					pool.query("INSERT INTO ct_trees (uniquename, species, genus, family, subkingdom, latitude, longitude, source_id, icon_type) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9) ON CONFLICT(uniquename) DO NOTHING",
						["treesnap." + tree_data["id"], tree_data["genus"] + " " + tree_data["species"], tree_data["genus"], null, null, tree_data["latitude"], tree_data["longitude"], 1, 4],
						function (err, result) {
							if (err) {
								console.log(err);
								return;
							}
							
							//console.log(tree_data['meta_data']);
							
							if(tree_data['meta_data'].diameterNumeric_values != undefined) {
								pool.query("INSERT INTO public.ct_trees_phenotypic_data (tree_acc,phenotype_name,value,units) VALUES ($1,$2,$3,$4)",
									["treesnap." + tree_data["id"], "diameter", tree_data['meta_data'].diameterNumeric_values.US_value, tree_data['meta_data'].diameterNumeric_values.US_unit], function(err, result) {
									if(err) {
										console.log(err);
										return;
									}
									//nothing to do
								});
							}
							
						}
					);
				}
				*/
			}
			//console.log(data.next_page_url);
			if (data.next_page_url != null) {
				treesnap_add(data.next_page_url);
			}
		}
		else {
			console.log('ERROR Response code from the API was:' + response.statusCode);
		}
	});
	return;
}

function treesnap_add_row(tree_data) {
	console.log('TreeSnap ID:' + tree_data["id"]);
	if (tree_data["species"] != "Unknown" && tree_data["genus"] != "Unknown") {

		fs.appendFileSync('meta_data.txt', JSON.stringify(tree_data['meta_data']) + "\n");

		pool.query("INSERT INTO ct_trees (uniquename, species, genus, family, subkingdom, latitude, longitude, source_id, icon_type) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9) ON CONFLICT(uniquename) DO NOTHING",
			["treesnap." + tree_data["id"], tree_data["genus"] + " " + tree_data["species"], tree_data["genus"], null, null, tree_data["latitude"], tree_data["longitude"], 1, 4],
			function (err, result) {
				if (err) {
					console.log(err);
					return;
				}
				
				//console.log(tree_data['meta_data']);
				if(tree_data['meta_data'].diameterNumeric_values != undefined) {
					pool.query("INSERT INTO public.ct_trees_phenotypic_data (uniquename, tree_acc,name,mapped_name,phenotype_name,value,units,observable_name,observable_accession,cvterm_name,cvterm_accession,structure_name,structure_accession) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13) ON CONFLICT(uniquename) DO NOTHING",
						["treesnap." + tree_data["id"] + ".diameter", "treesnap." + tree_data["id"],"diameter",'Diameter',"diameter", tree_data['meta_data'].diameterNumeric_values.US_value, tree_data['meta_data'].diameterNumeric_values.US_unit,"diameter","PATO:0001334","diameter","PATO:0001334","stem", "PO_0009047"], function(err, result) {
						if(err) {
							console.log(err);
							return;
						}
						//nothing to do
					});
				}
				if(tree_data['meta_data'].heightNumeric_values != undefined) {
					pool.query("INSERT INTO public.ct_trees_phenotypic_data (uniquename, tree_acc,name,mapped_name,phenotype_name,value,units,observable_name,observable_accession,cvterm_name,cvterm_accession,structure_name,structure_accession) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13) ON CONFLICT(uniquename) DO NOTHING",
						["treesnap." + tree_data["id"] + ".height", "treesnap." + tree_data["id"],"height",'Height',"height", tree_data['meta_data'].heightNumeric_values.US_value, tree_data['meta_data'].heightNumeric_values.US_unit,"height","PATO:0000119","height","PATO:0000119","whole plant", "PO:0000003"], function(err, result) {
						if(err) {
							console.log(err);
							return;
						}
						//nothing to do
					});
				}		
				
				//Maybe sort this by descending...
				
				Object.keys(tree_data['meta_data']).forEach(function(key,index) {
					// key: the name of the object key
					// index: the ordinal position of the key within the object 
					if(Array.isArray(tree_data['meta_data'][key]) !== true && typeof tree_data['meta_data'][key] !== 'object') {
						var phenotype_name = key;
						var value = tree_data['meta_data'][key];
						var units = '-';
						var confidence = '-';
						//console.log(phenotype_name + ":" + value);
						

						if(tree_data['meta_data'][phenotype_name + '_units'] && phenotype_name.includes('_units') == false) {
							//this should be the basic phenotype with the value, so value variable is correct
							if(tree_data['meta_data'][phenotype_name + '_units'] != undefined) {
								units = tree_data['meta_data'][phenotype_name + '_units'];
							}
							if(tree_data['meta_data'][phenotype_name + '_confidence'] != undefined) {
								if(tree_data['meta_data'][phenotype_name + '_confidence'] != null) {
									confidence = tree_data['meta_data'][phenotype_name + '_confidence'];
								}
							}
						}

						if(phenotype_name.includes('_units') == true || phenotype_name.includes('_confidence') == true) {
							phenotype_name = 'ignore';
						}

	
						if(phenotype_name != 'ignore') {
							//console.log('INSERTING phenotype_name:' + phenotype_name);
							var mapped_name = phenotype_name;
							var new_units = units;
							var structure = '';
							var structure_accession = '';
							var attribute = '';
							var attribute_accession = '';
							var additional_term = '';
							var additional_term_accession = '';
							
							switch(phenotype_name) {
								case 'acorns':
									mapped_name = 'Acorns present';
									new_units = 'ordinal';
									structure = 'seed';
									structure_accession = 'PO_0009010';
									attribute = 'amount';
									attribute_accession = 'PATO_0000070';
									additional_term = '';
									additional_term_accession = '';
									break;
								case 'burrs':
									mapped_name = 'Burrs present';
									new_units = 'ordinal';
									structure = 'seed';
									structure_accession = 'PO_0009010';
									attribute = 'amount';
									attribute_accession = 'PATO_0000070';
									additional_term = '';
									additional_term_accession = '';
									break;
								case 'catkins':
									mapped_name = 'Catkins present';
									new_units = 'binary';
									structure = 'floral organ';
									structure_accession = 'PO_0025395';
									attribute = 'amount';
									attribute_accession = 'PATO_0000070';
									additional_term = '';
									additional_term_accession = '';
									break;
								case 'chestnutBlightSigns':
									mapped_name = 'Signs of chestnut blight';
									new_units = 'qualitative';
									structure = 'whole plant';
									structure_accession = 'PO_0000003';
									attribute = 'damage';
									attribute_accession = 'PATO_0001020';
									additional_term = 'Cryphonectria parasitica';
									additional_term_accession = 'NCBITaxon_511';
									break;
								case 'cones':
									mapped_name = 'Cones present';
									new_units = 'binary';
									structure = 'floral organ';
									structure_accession = 'PO_0025395';
									attribute = 'amount';
									attribute_accession = 'PATO_0000070';
									additional_term = '';
									additional_term_accession = '';
									break;
								case 'conesMaleFemale':
									mapped_name = 'Male or female cones present';
									new_units = 'ordinal';
									structure = 'floral organ';
									structure_accession = 'PO_0025395';
									attribute = 'amount';
									attribute_accession = 'PATO_0000070';
									additional_term = '';
									additional_term_accession = '';
									break;	
								case 'crownClassification':
									mapped_name = 'Crown position relative to nearby trees';
									new_units = 'qualitative';
									structure = 'shoot system';
									structure_accession = 'PO_0009006';
									attribute = 'position';
									attribute_accession = 'PATO_0000140';
									additional_term = '';
									additional_term_accession = '';
									break;
								case 'crownHealth':
									mapped_name = 'Crown health';
									new_units = 'ordinal';
									structure = 'shoot system';
									structure_accession = 'PO_0009006';
									attribute = 'damage';
									attribute_accession = 'PATO_0001020';
									additional_term = '';
									additional_term_accession = '';
									break;	
								case 'deerRub':
									mapped_name = 'Damage from deer rubbing';
									new_units = 'ordinal';
									structure = 'stem';
									structure_accession = 'PO_0009047';
									attribute = 'damage';
									attribute_accession = 'PATO_0001020';
									additional_term = 'Cervidae';
									additional_term_accession = 'NCBITaxon_9850';
									break;	
								case 'diameter':
									mapped_name = 'Diameter';
									new_units = units;
									structure = 'stem';
									structure_accession = 'PO_0009047';
									attribute = 'diameter';
									attribute_accession = 'PATO_0001334';
									additional_term = '';
									additional_term_accession = '';
									break;
								case 'emeraldAshBorer':
									mapped_name = 'Signs of Emerald Ash Borer';
									new_units = 'qualitative';
									structure = 'whole plant';
									structure_accession = 'PO_0000003';
									attribute = 'damage';
									attribute_accession = 'PATO_0001020';
									additional_term = 'Agrilus planipennis';
									additional_term_accession = 'NCBITaxon_224129';
									break;	
								case 'flowersBinary':
									mapped_name = 'Flowers present (binary)';
									new_units = 'binary';
									structure = 'floral organ';
									structure_accession = 'PO_0025395';
									attribute = 'amount';
									attribute_accession = 'PATO_0000070';
									additional_term = '';
									additional_term_accession = '';
									break;	
								case 'height':
									mapped_name = 'Height';
									new_units = units;
									structure = 'whole plant';
									structure_accession = 'PO_0000003';
									attribute = 'height';
									attribute_accession = 'PATO_0000119';
									additional_term = '';
									additional_term_accession = '';
									break;	
								case 'heightFirstBranch':
									mapped_name = 'Height of first branch';
									new_units = units;
									structure = 'whole plant';
									structure_accession = 'PO_0000003';
									attribute = 'height';
									attribute_accession = 'PATO_0000120';
									additional_term = '';
									additional_term_accession = '';
									break;	
								case 'locationCharacteristics':
									mapped_name = 'Location type';
									new_units = 'qualitative';
									structure = 'whole plant';
									structure_accession = 'PO_0000003';
									attribute = 'ecological environment exposure';
									attribute_accession = 'PECO:0007064';
									additional_term = '';
									additional_term_accession = '';
									break;	
								case 'nearbyTrees':
									mapped_name = 'Health and size of nearby trees';
									new_units = 'qualitative';
									structure = 'whole plant';
									structure_accession = 'PO_0000003';
									attribute = 'nearby tree health';
									attribute_accession = 'tg 57';
									additional_term = '';
									additional_term_accession = '';
									break;
								case 'needleAmount':
									mapped_name = 'Needle amount';
									new_units = 'binary';
									structure = 'vascular leaf';
									structure_accession = 'PO_0009025';
									attribute = 'amount';
									attribute_accession = 'PATO_0000070';
									additional_term = '';
									additional_term_accession = '';
									break;
								case 'needleColor':
									mapped_name = 'Needle color';
									new_units = 'qualitative';
									structure = 'vascular leaf';
									structure_accession = 'PO_0009025';
									attribute = 'color';
									attribute_accession = 'PATO_0000014';
									additional_term = '';
									additional_term_accession = '';
									break;		
								case 'numberRootSprouts':
									mapped_name = 'Number of root sprouts';
									new_units = 'discrete';
									structure = 'shoot system';
									structure_accession = 'PO_0009006';
									attribute = 'has number of';
									attribute_accession = 'PATO_0001555';
									additional_term = '';
									additional_term_accession = '';
									break;
								case 'oakHealthProblems':
									mapped_name = 'Signs of disease (oak)';
									new_units = 'qualitative';
									structure = 'whole plant';
									structure_accession = 'PO_0000003';
									attribute = 'damage';
									attribute_accession = 'PATO_0001020';
									additional_term = '';
									additional_term_accession = '';
									break;
								case 'plantedWild':
									mapped_name = 'Wild or human-planted';
									new_units = 'binary';
									structure = 'whole plant';
									structure_accession = 'PO_0000003';
									attribute = 'plant placement source';
									attribute_accession = 'tg 58';
									additional_term = '';
									additional_term_accession = '';
									break;
								case 'seedsBinary':
									mapped_name = 'Seeds present (binary)';
									new_units = 'binary';
									structure = 'seed';
									structure_accession = 'PO_0009010';
									attribute = 'amount';
									attribute_accession = 'PATO_0000070';
									additional_term = '';
									additional_term_accession = '';
									break;	
								case 'standTagging':
									mapped_name = 'Stand tagged (binary)';
									new_units = 'binary';
									structure = 'whole plant';
									structure_accession = 'PO_0000003';
									attribute = 'labeled';
									attribute_accession = 'NCIT_C43386';
									additional_term = '';
									additional_term_accession = '';
									break;
								case 'torreyaFungalBlight':
									mapped_name = 'Signs of fungal blight on Torreya';
									new_units = 'qualitative';
									structure = 'whole plant';
									structure_accession = 'PO_0000003';
									attribute = 'damage';
									attribute_accession = 'PATO_0001020';
									additional_term = 'Physalospora';
									additional_term_accession = 'NCBITaxon_186525';
									break;
								case 'treated':
									mapped_name = 'Treated (binary)';
									new_units = 'binary';
									structure = 'whole plant';
									structure_accession = 'PO_0000003';
									attribute = 'plant exposure';
									attribute_accession = 'PECO:0001001';
									additional_term = '';
									additional_term_accession = '';
									break;
								case 'woollyAdesCoverage':
									mapped_name = 'Coverage of infection by wooly adelgid';
									new_units = 'qualitative';
									structure = 'whole plant';
									structure_accession = 'PO_0000003';
									attribute = 'amount';
									attribute_accession = 'PATO_0000070';
									additional_term = 'Adelges tsugae';
									additional_term_accession = 'NCBITaxon_357502';
									break;										
							}
							
							if(value != "I'm not sure" && value != "Unknown" && value != "Not sure" && phenotype_name != "ashSpecies") {
								console.log(phenotype_name + ',' + value + ',' + new_units);
								pool.query("INSERT INTO public.ct_trees_phenotypic_data (uniquename, tree_acc,name,phenotype_name,mapped_name,value,units,observable_name,observable_accession,cvterm_name,cvterm_accession,structure_name,structure_accession,confidence,additional_term,additional_term_accession) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16) ON CONFLICT(uniquename) DO NOTHING",
									["treesnap." + tree_data["id"] + "." + phenotype_name, "treesnap." + tree_data["id"],phenotype_name,phenotype_name,mapped_name,value,new_units,attribute,attribute_accession,phenotype_name,"",structure, structure_accession,confidence,additional_term,additional_term_accession], function(err, result) {
									if(err) {
										console.log(err);
										return;
									}
									//nothing to do
								});
							}
						}
						
					}
				});			
				
			}
		);
	}	
}

function execute_request(query_str, query_args, response, next) {
	pool.query (query_str, query_args, function(err, result) {
		if (err) {
			console.log(err);
			response.status(400);
			return;
		}
		response.status(200).json(result.rows);
		return;
	});
}

function valid_api_key(key, response, callback) {
	var encrypted_key = encrypt(key).toString("hex");
	//console.log(key);
	//console.log(encrypted_key);
	pool.query("SELECT * FROM ct_api_keys WHERE api_key = $1", [encrypted_key], function (err, result) {
		if (err || result.rowCount == 0) {
			//console.log("Invalid API key: " + result.rowCount); //this causes the server 'crash'
			console.log("Invalid API key");
			console.log(err);
			response.status(401).json("Invalid API key supplied");
			return;
		}
		callback();
		return;
	});
}

function encrypt(string) {
	try {
		var cipher = crypto.createCipher("aes-256-cbc", process.env.TOKEN_PW);
		var encrypted = cipher.update(string, "utf8", "base64");
		encrypted += cipher.final("base64");
		return encrypted;
	} 
	catch (exception) {
		return exception.message;
	}
}
	
function decrypt(string) {
	try {
		var decipher = crypto.createDecipher("aes-256-cbc", process.env.TOKEN_PW);
		var decrypted = decipher.update(string, "base64", "utf8");
		decrypted += decipher.final("utf8");
		return decrypted;
	} 
	catch (exception) {
		return exception.message;
	}
}


function generate_date_time() {
	var today = new Date();
	var date = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();
	var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();

	return date_time = date + " " + time;
}

/**
 * The parser for the json object created from the query builder
 * @param {JSON object} data - the json object created from the q builder
 * @param {string} qString - the sql query string
 */
function parse_json_query(data, qString) {
	var opMapping = {"equal": "=", "not equal": "<>"}

	if (Object.keys(data).length == 0) {
		return "";
	}
	var cond = data["condition"];
	var rules = data["rules"];
	qString += "(";
	for (var i = 0; i < rules.length; i++) {
		if (rules[i]["condition"] != undefined) {
			//qString = parseJsonQuery(rules[i], qString); //OLD
			qString = parse_json_query(rules[i], qString); //BUG FIX? The name of the function was named incorrectly?
		}
		else {
			if(rules[i]["field"] == "accession") {
				qString += "uniquename ILIKE '" + rules[i]["value"] + "%'"; 
			}
			else {
				qString += rules[i]["field"] + " " + opMapping[rules[i]["operator"]] + " '" + rules[i]["value"] + "'";
			}
		}
		
		if (i < rules.length - 1) {
			qString += " " + cond + " ";
		}	
	}
	qString += ")";
	return qString;
}

/**
 * The parser for the the single type, but potentially multiple combination selection for the filter fields
 * @param {JSON object} query_object - the json object created from the q builder
 * @param {boolean} match - indicator to use ILIKE or check for equlity in query
 * @param {string} field - the chosen field to base the filter on
 */
function parse_uni_query(query_object, match, field) {
	var base_query = "";//"SELECT uniquename FROM ct_trees WHERE";	
	var matching = [];
	var k ="";
	var clause = match ? "ILIKE" : "=";

	if (typeof(query_object) == "object") {	
		var obj_keys = Object.keys(query_object).sort(function(a,b){return (query_object[a]).localeCompare(query_object[b])});
		for (var i = 0; i < obj_keys.length; i++) {
			base_query += " " + field + " " + clause + " $" + (i + 1);
			if (i < obj_keys.length - 1) {
				base_query += " OR";
			}
			k += query_object[obj_keys[i]];
			
			if (match) {
				query_object[obj_keys[i]] += "%";
			}
			matching.push(query_object[obj_keys[i]]);
		}
	}
	else {
		if (match) {
			matching.push(query_object + "%");
		}
		else {
			matching.push(query_object);
		}
		k += query_object;
		base_query += " " + field + " "  + clause + " $1";
	}
	return {"key": k, "query_vals": matching, "query_str": base_query}; 
}

/**
 * Returns a list of trees along with their basic properties
 * @param {object} response - Response object
 * @param {object} query_props the query properties
 */
function get_trees(response, query_props) {
	pool.query (query_props["query_str"], query_props["query_vals"], function(err, result) {
		if (err) {
			//res.json("bam baby");
			return err;
		}
		var rows = result.rows;
		var treeFeatures = [];
		for (var i = 0; i < rows.length; i++) {
			var currTree = {"type": "Feature"};
			currTree["properties"] = {"id": rows[i]["uniquename"], "icon": 0};
			currTree["geometry"] = {"type": "point", "coordinates": [rows[i]["longitude"], rows[i]["latitude"]]};
			treeFeatures.push(currTree);
		}
		ct_cache.put(query_props["key"] + "_trees", treeFeatures, cache_time_24);
		response.status(200).json(treeFeatures);
	});
}

/**
 * Returns a list of trees identified by their tree_num
 * @param {object} response - Response object
 * @param {object} query_props the query properties
 */
function get_tree_ids(response, query_props) {
	pool.query (query_props["query_str"], query_props["query_vals"], function(err, result) {
		if (err) {
			return err;
		}
		var rows = result.rows;
		var trees = [];
		for (var i = 0; i < rows.length; i++) {
			trees.push(rows[i]["tree_num"]);
		}
		ct_cache.put(query_props["key"] + "_trees", trees, cache_time_24);
		response.status(200).json(trees);
	});
}

function insert_trees_query(res, tree, icon_type, coord_type) {
	pool.query("INSERT INTO ct_trees (uniquename, genus, species, subkingdom, family, latitude, longitude, coordinate_type, source_id, icon_type) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) ON CONFLICT (uniquename) DO NOTHING", [tree["uniquename"], tree["genus"], tree["genus"] + " " + tree["species"], tree["subkingdom"], tree["family"], tree["latitude"], tree["longitude"], coord_type, 0, icon_type], function (err, result) {
		if (err) {
			//res.status(200).json(err.stack);
		}
		//console.log('.');
	});
}

function insert_new_trees(res, trees_arr) {
	for (var i = 0; i < trees_arr.length; i++) {
		var tree = trees_arr[i];
		var icon_type = 0;
		var coord_type = tree["coordinate_type"] == "exact" ? 0 : 1;
		if (tree["subkingdom"] == "angiosperm") {
			if (coord_type == 0) {
				icon_type = 0;
			}	
			else {
				icon_type = 2;
			}
		}
		else {	
			if (coord_type == 0) {
				icon_type = 1;
			}	
			else {
				icon_type = 3;
			}
		}
		insert_trees_query(res, tree, icon_type, coord_type);
	}
	//pool.end();
	//res.status(200).json("He's just standing there ... MENACINGLY!!!");	
	//return;
}

module.exports = {
	get_trees,
	get_tree_ids,
	parse_uni_query,
	parse_json_query,	
	insert_new_trees,
	generate_date_time,
	encrypt,
	decrypt,
	valid_api_key,
	execute_request,
	treesnap_add,
}
