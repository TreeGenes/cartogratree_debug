var express = require("express");
var responseTime = require("response-time");
var bodyParser = require("body-parser");
const WebSocket = require('ws');//websocket implementation
var Worker = require('webworker-threads').Worker;
const pg = require('pg');//Postgres database implementation
const isSea = require('is-sea');
const QueryStream = require('pg-query-stream');//Postgres Query Streaming implementation
const ct_cache = require("memory-cache");
const delay = require('delay');
var LZUTF8 = require('lzutf8');
var fs = require('fs');
var write_source_id_geojson_file = false; //this will force the function to write the geojson file
var cors = require("cors")
var app = express();
var port = process.env.PORT || 8088;



var dotenv = require("dotenv")

const result = dotenv.config();

global.__basedir = __dirname;

if (result.error) {
  throw result.error;
}
console.log('FROM DOTENV:');
console.log(result.parsed);

var logger = function(req, res, next) {
	console.log(req.url);
	next(); // Passing the request to the next handler in the stack.
}

app.use(logger);
app.use(cors());
app.use(responseTime());
app.use(bodyParser.json());
//app.use(bodyParser.urlencoded({extended: false}));

app.set("json spaces", 0);

var routes = require("./routes");	// import routes
routes(app);						// register routes

app.listen(port, "0.0.0.0");

app.use(function(req, res, next) {
	next(createError(404));
});
// address unhandled urls
app.use(function(err, req, res, next) {
	res.locals.message = err.message;
	
	res.status(err.status || 500).send({url: req.originalUrl + " not found"});
});

console.log("CartograTree HTTP API server started on: " + port);


const wss = new WebSocket.Server({ port: 8880, host: '0.0.0.0' });
console.log("Cartogratree Websocket API server started on: 8880");
var user_count = 0;
//The Websocket server object that does most functions
var ctapi_wss = {
	
	//An array with current clients connected
	clients: new Array(),
	
	//This function essentially boots up the ctapi_wss server for performing
	//commands on connection and messages
	
	start: function() {
		wss.on('connection', function connection(wss, req) {
			wss.send('NOTICE:Successfully connected.');
			var client_id = user_count + 1;
			user_count = user_count + 1;
			ctapi_wss.addClient(wss, client_id);

			wss.on('message', function incoming(message) {
				message = message.replace(/(\r\n|\n|\r)/gm, "");
				//console.log('Received from userid: %s', user_count);
				
				ctapi_wss.processMessage(wss, message);
			});

			wss.on('close', function() {
				ctapi_wss.removeClient(wss);
			});				
		});
	},

	//This will process messages received
	processMessage: function(wss, message) {
		var message_parts_init = ctapi_wss.split(message, ':', 1);
		//console.log(message_parts_init);
		switch(message_parts_init[0]) {
			case 'hello':
				wss.send('hello back at you');
				break;
			case 'trees_by_source_id':
				//wss.send('hello back at you');
				ctapi_wss.api_trees_by_source_id(wss, message);
				break;
			case 'trees_by_source_id_linked_treegenes_and_dryad':
				ctapi_wss.api_trees_by_source_id_linked_treegenes_and_dryad(wss,message);
				break;
		}
	},
	
	//API function to get trees by source_id and stream it back to the browser
	api_trees_by_source_id: function(wss, message) {
		try {
			var msg_parts = ctapi_wss.split(message, ':', 1);
			var source_id = msg_parts[1];
			if(source_id == null) {
				source_id = 0;
			}
			else {
				source_id = parseInt(source_id);
			}
			//This subsection gets the count for the source_id specified
			pg_pool.connect((err, pg_client, done) => {
				if(err) {
					console.log('Error in api_trees_by_source_id' + err);
					done();
				}
				else {
					//if (err) throw err;
					var query = new QueryStream('SELECT count(*) as c FROM public.ct_trees WHERE source_id = $1 AND (latitude BETWEEN 23.885838 AND 48.9222499) AND (longitude BETWEEN -125.595703 AND -66.884766)',[source_id]);
					const stream = pg_client.query(query);
					var count = 0;
					wss.send('trees_by_source_id_total_tree_count_start:' + source_id);
					stream.on('end', function() {
						done();
						console.log('api_trees_by_source_id DONE clause');
					});					
					stream.on('data', (row) => {
						//console.log(row);
						try {
							var tree_count = row.c.toString().trim();
							console.log('trees_by_source_id_total_tree_count:' + source_id + ':' + tree_count);
							wss.send('trees_by_source_id_total_tree_count:' + source_id + ':' + tree_count); //this send response to the browser
						}
						catch(err) {
							console.log(err);
						}
					});				
				}
			});
			
			//Once we know the count from the previous section, we should now stream the actual results
			pg_pool.connect((err, pg_client, done) => {
				if(err) {
					console.log('Error in api_trees_by_source_id' + err);
					done();
				}
				else {
					//if (err) throw err;
					var query = new QueryStream('SELECT * FROM public.ct_trees WHERE source_id = $1 AND (latitude BETWEEN 23.885838 AND 48.9222499) AND (longitude BETWEEN -125.595703 AND -66.884766) LIMIT 1000000',[source_id]);
					const stream = pg_client.query(query);
					var count = 0;
					wss.send('trees_by_source_id_start:' + source_id);
					stream.on('end', function() {
						wss.send('trees_by_source_id_stop:' + source_id);
						done();
						console.log('api_trees_by_source_id DONE clause');
					});					
					stream.on('data', (row) => {
						//console.log(row);
						try {
							count = count + 1;
							var uniquename = row.uniquename.toString().trim();
							var longitude = row.longitude.toString().trim();
							var latitude = row.latitude.toString().trim();
							var json_text = '{"type":"Feature","properties":{"id":"' + uniquename + '","icon_type":0},"geometry":{"type":"point","coordinates":[' + longitude + ',' + latitude + ']}}';
							//json_text = LZUTF8.compress(json_text, {outputEncoding: "StorageBinaryString"});
							//Try to make this easy by creating a json object
							
							wss.send('tbsiit::' + source_id + '::' + count + '::' + json_text);
						}
						catch(err) {
							console.log(err);
						}
					});				
				}
			});
		}
		catch(err) {
			console.log('Error with function get_country_details:' + err.toString());
		}		
	},
	
	//Create POSTGIS table for bien trees
	api_create_postgis_bien_table: function(wss, message) {
		var prefix = '[api_trees_by_source_id_linked_treegenes_and_dryad] ';
		try {
			var msg_parts = ctapi_wss.split(message, ':', 1);
			var source_id = msg_parts[1];
			if(source_id == null) {
				source_id = 0;
			}
			else {
				source_id = parseInt(source_id);
			}
			
			/*
			var ct_trees_segment_count = ct_cache.get("ct_trees_source_id_" + source_id + "_segment_count");
			console.log("Segment count:" + ct_trees_segment_count);
			if(ct_trees_segment_count == undefined || ct_trees_segment_count == null) {
			*/
			if(true) {
				pg_pool.connect((err, pg_client, done) => {
					if(err) {
						console.log('Error in api_trees_by_source_id' + err);
						done();
					}
					else {
						
						//if (err) throw err;
						var query = new QueryStream('SELECT distinct(species) as species FROM public.ct_trees WHERE source_id = $1 OR source_id = $2;',[0,2]);
						const stream = pg_client.query(query);
						var species_array = [];
						//wss.send('trees_by_source_id_total_tree_count_start:' + source_id);
						stream.on('end', function() {
							//done();
							
							var rows_for_cache = [];
							var iteration = 1;
							//foreach species in species_array
							console.log(prefix + 'Found ' + species_array.length + ' linked unique species from TreeGenes and DryAd combined');
							//wss.send('trees_by_source_id_start:' + source_id);
							var i = 0;
							var count = 0;
							while (i<species_array.length) {
								(function(i) {
									var current_species = species_array[i];
									//perform subquery to get the trees
									//console.log(source_id + ':' + current_species);
									var query_species = new QueryStream('SELECT uniquename, longitude, latitude, genus, species, family FROM public.ct_trees WHERE source_id = $1 AND species LIKE $2;', [source_id, current_species]);
									var stream_species = pg_client.query(query_species);
									stream_species.on('end', function() {
										if(i == species_array.length - 1) {

											
											//wss.send('trees_by_source_id_stop:' + source_id);
																				
											done();
											console.log(prefix + 'All results returned to client');
										}
										else {
											console.log(prefix + 'Finished retrieving results for ' + current_species);
										}
									});
									stream_species.on('data', (row_species) => {
										//console.log(row);
										try {
											
											count = count + 1;
											
											
											//console.log(count);
											//console.log(row_species.uniquename.toString().trim());
											var uniquename = row_species.uniquename.toString().trim();
											var longitude = row_species.longitude.toString().trim();
											var latitude = row_species.latitude.toString().trim();
											var genus = row_species.genus.toString().trim();
											var species = row_species.species.toString().trim();
											var family = row_species.family.toString().trim();
											//if(parseFloat(latitude) != 0 && parseFloat(longitude) != 0) { //not a sea point (the data is valid so send it)
												//Try to make this easy by creating a json object
											
												//This is uncompressed
												//var json_text_uncmp = '{"type":"Feature","properties":{"id":"' + uniquename + '","icon_type":0,"genus":"' + genus + '", "species":"' + species + '", "family":"' + family + '"},"geometry":{"type":"Point","coordinates":[' + longitude + ',' + latitude + ']}}';
												
												//Modified simple compression
												//var json_text = 'ex1' + uniquename + 'ex2' + longitude + ',' + latitude + ']}}';

												//wss.send('tbsiit::' + source_id + '::' + count + '::' + json_text);
											//}	
										}
										catch(err) {
											console.log(err);
										}
									});	
								})(i);
								i = i + 1;
							}
						});					
						stream.on('data', (row) => {
							//console.log(row);
							try {
								console.log(row.species.toString().trim());
								species_array.push(row.species.toString().trim());
							}
							catch(err) {
								console.log(err);
							}
						});				
					}
				});
			}
		}
		catch(err) {
			console.log('Error with function get_country_details:' + err.toString());
		}		
	},
	
	
	//API function to get trees by source_id and stream it back to the browser
	api_trees_by_source_id_linked_treegenes_and_dryad: function(wss, message) {
		var prefix = '[api_trees_by_source_id_linked_treegenes_and_dryad] ';
		try {
			var msg_parts = ctapi_wss.split(message, ':', 1);
			var source_id = msg_parts[1];
			if(source_id == null) {
				source_id = 0;
			}
			else {
				source_id = parseInt(source_id);
			}
			
			/*
			var ct_trees_segment_count = ct_cache.get("ct_trees_source_id_" + source_id + "_segment_count");
			console.log("Segment count:" + ct_trees_segment_count);
			if(ct_trees_segment_count == undefined || ct_trees_segment_count == null) {
			*/
			if(true) {
				pg_pool.connect((err, pg_client, done) => {
					if(err) {
						console.log('Error in api_trees_by_source_id' + err);
						done();
					}
					else {
						//Delete the file if it exists to regenerate it
						try {
							if (write_source_id_geojson_file == true) {
								fs.unlink('geojson.json',
									// callback function
									function(err) { 
										if (err) {
											console.log(err);
										}
										// if no error
										//console.log("Data is appended to file successfully.")
									}	
								);	
							}
						}	
						catch(err) {
							console.log(err);
						}						
						
						
						//if (err) throw err;
						var query = new QueryStream('SELECT distinct(species) as species FROM public.ct_trees WHERE source_id = $1 OR source_id = $2;',[0,2]);
						const stream = pg_client.query(query);
						var species_array = [];
						//wss.send('trees_by_source_id_total_tree_count_start:' + source_id);
						stream.on('end', function() {
							//done();
							try {
								if (write_source_id_geojson_file == true) {
									fs.appendFile('geojson.json','[', 'utf8',
										// callback function
										function(err) { 
											if (err) {
												console.log(err);
											}
											// if no error
											//console.log("Data is appended to file successfully.")
										}
									);	
								}
							}	
							catch(err) {
								console.log(err);
							}						
							
							var rows_for_cache = [];
							var iteration = 1;
							//foreach species in species_array
							console.log(prefix + 'Found ' + species_array.length + ' linked unique species from TreeGenes and DryAd combined');
							wss.send('trees_by_source_id_start:' + source_id);
							var i = 0;
							var count = 0;
							while (i<species_array.length) {
								(function(i) {
									var current_species = species_array[i];
									//perform subquery to get the trees
									//console.log(source_id + ':' + current_species);
									var query_species = new QueryStream('SELECT uniquename, longitude, latitude, genus, species, family FROM public.ct_trees WHERE source_id = $1 AND species LIKE $2;', [source_id, current_species]);
									var stream_species = pg_client.query(query_species);
									stream_species.on('end', function() {
										if(i == species_array.length - 1) {
											//Set cache
											/*
											ct_cache.put("ct_trees_source_id_" + source_id + "_" + iteration, rows_for_cache, 7*24*60*60*1000); //Usually 7 days
											ct_cache.put("ct_trees_source_id_" + source_id + "_segment_count", iteration, 7*24*60*60*1000); //Usually 7 days
											wss.send('Rows cached:' + count);
											wss.send('Segments cached:' + count);
											*/
											
											wss.send('trees_by_source_id_stop:' + source_id);
											
											try {
												if (write_source_id_geojson_file == true) {
													fs.appendFile('geojson.json',']', 'utf8',
														// callback function
														function(err) { 
															if (err) {
																console.log(err);
															}
															// if no error
															//console.log("Data is appended to file successfully.")
														}
													);	
												}
											}	
											catch(err) {
												console.log(err);
											}										
											done();
											console.log(prefix + 'All results returned to client');
										}
										else {
											console.log(prefix + 'Finished retrieving results for ' + current_species);
										}
									});
									stream_species.on('data', (row_species) => {
										//console.log(row);
										try {
											
											count = count + 1;
											
											//Cache the specific segment
											/*
											rows_for_cache.push(row_species);
											if(count % 500000 == 0 && count > 0) {
												ct_cache.put("ct_trees_source_id_" + source_id + "_" + iteration, rows_for_cache, 7*24*60*60*1000); //Usually 7 days
												console.log("ct_trees_source_id_" + source_id + "_" + iteration + " segments has been saved in cache");
												iteration = iteration + 1;
												rows_for_cache = []; //reset the array
											}
											*/
											
											
											//console.log(count);
											//console.log(row_species.uniquename.toString().trim());
											var uniquename = row_species.uniquename.toString().trim();
											var longitude = row_species.longitude.toString().trim();
											var latitude = row_species.latitude.toString().trim();
											var genus = row_species.genus.toString().trim();
											var species = row_species.species.toString().trim();
											var family = row_species.family.toString().trim();
											//if(parseFloat(latitude) != 0 && parseFloat(longitude) != 0) { //not a sea point (the data is valid so send it)
												//Try to make this easy by creating a json object
											
												//This is uncompressed
												var json_text_uncmp = '{"type":"Feature","properties":{"id":"' + uniquename + '","icon_type":0,"genus":"' + genus + '", "species":"' + species + '", "family":"' + family + '"},"geometry":{"type":"Point","coordinates":[' + longitude + ',' + latitude + ']}}';
												
												//Modified simple compression
												var json_text = 'ex1' + uniquename + 'ex2' + longitude + ',' + latitude + ']}}';
												
												// append data to file
												try {
													if (write_source_id_geojson_file == true) {
														if(count == 1) {
															fs.appendFile('geojson.json',json_text_uncmp, 'utf8',
																// callback function
																function(err) { 
																	if (err) {
																		console.log(err);
																	}
																	// if no error
																	//console.log("Data is appended to file successfully.")
																}
															);	
														}
														else {
															fs.appendFile('geojson.json',',' + json_text_uncmp, 'utf8',
																// callback function
																function(err) { 
																	if (err) {
																		console.log(err);
																	}
																	// if no error
																	//console.log("Data is appended to file successfully.")
																}
															);	
														}
													}
												}	
												catch(err) {
													console.log(err);
												}
												wss.send('tbsiit::' + source_id + '::' + count + '::' + json_text);
											//}	
										}
										catch(err) {
											console.log(err);
										}
									});	
								})(i);
								i = i + 1;
							}
						});					
						stream.on('data', (row) => {
							//console.log(row);
							try {
								console.log(row.species.toString().trim());
								species_array.push(row.species.toString().trim());
							}
							catch(err) {
								console.log(err);
							}
						});				
					}
				});
			}
			/*
			else {
				//it found cache data so use that instead of pulling from database
				console.log('Found ct_trees_source_id_' + source_id + ' from cache - attempting to stream to user interface...');
				console.log('Segments found:' + ct_trees_segment_count);
				var sendMessage = new Worker(function() {

					this.onmessage = function (event) {
					  //postMessage(sendMessage(event.data));
					  //console.log(event.data.msg);
					  (async () => {
						wss.send(event.data.msg);
					  })();
					}
				});

		
				//Send header
				wss.send('trees_by_source_id_start:' + source_id);
				//(async () => {
						//bar();				
					for(var j=1; j<= ct_trees_segment_count; j++) {
						var rows_from_cache = ct_cache.get("ct_trees_source_id_" + source_id + "_" + j);
						console.log('Attempting to send ct_trees_source_id_' + source_id + ' segment ' + j + ' of ' + ct_trees_segment_count + ' from cache');

							for(var i=0; i < rows_from_cache.length; i++) {
								try {
									
									//process and send it out per row
									var row_species = rows_from_cache[i];
									var uniquename = row_species.uniquename.toString().trim();
									var longitude = row_species.longitude.toString().trim();
									var latitude = row_species.latitude.toString().trim();					
									var json_text = '{"type":"Feature","properties":{"id":"' + uniquename + '","icon_type":0},"geometry":{"type":"point","coordinates":[' + longitude + ',' + latitude + ']}}';
									//console.log(i);
									//wss.send('tbsiit::' + source_id + '::' + (i+1) + '::' + json_text);
									sendMessage.postMessage({'msg': 'tbsiit::' + source_id + '::' + (i+1) + '::' + json_text});
									
								}
								catch(err) {
									console.log(err);
								}
								
							}
							console.log('Waiting 1 second...');
							//await delay(1000);
						
					}
								
					
					//Send ender
					wss.send('trees_by_source_id_stop:' + source_id);
					console.log('Stream completed.');
				//})();				
			}
			*/
			
			/*
			//Once we know the count from the previous section, we should now stream the actual results
			pg_pool.connect((err, pg_client, done) => {
				if(err) {
					console.log('Error in api_trees_by_source_id' + err);
					done();
				}
				else {
					//if (err) throw err;
					var query = new QueryStream('SELECT * FROM public.ct_trees WHERE source_id = $1 AND (latitude BETWEEN 23.885838 AND 48.9222499) AND (longitude BETWEEN -125.595703 AND -66.884766) LIMIT 1000000',[source_id]);
					const stream = pg_client.query(query);
					var count = 0;
					wss.send('trees_by_source_id_start:' + source_id);
					stream.on('end', function() {
						wss.send('trees_by_source_id_stop:' + source_id);
						done();
						console.log('api_trees_by_source_id DONE clause');
					});					
					stream.on('data', (row) => {
						//console.log(row);
						try {
							count = count + 1;
							var uniquename = row.uniquename.toString().trim();
							var longitude = row.longitude.toString().trim();
							var latitude = row.latitude.toString().trim();
							var json_text = '{"type":"Feature","properties":{"id":"' + uniquename + '","icon_type":0},"geometry":{"type":"point","coordinates":[' + longitude + ',' + latitude + ']}}';
							//Try to make this easy by creating a json object
							
							wss.send('trees_by_source_id_individual_tree::' + source_id + '::' + count + '::' + json_text);
						}
						catch(err) {
							console.log(err);
						}
					});				
				}
			});
			*/
		}
		catch(err) {
			console.log('Error with function get_country_details:' + err.toString());
		}		
	},
	

	//This adds a new client to the clients array
	addClient: function(wss, client_id){
		var client_object = { socket: wss, client_id: client_id, client_type: 'user', client_username: 'N/A', client_current_page: 'N/A', client_ip: 'N/A'};
		ctapi_wss.clients.push(client_object);
	},	
	
	//This removes a client from the clients array
	removeClient: function(wss) {
		for(i = 0; i < ctapi_wss.clients.length; i++){
			if(ctapi_wss.clients[i].socket === wss){
				ctapi_wss.clients.splice(i,1);
				//console.log('remove client');
			}
		}		
	},
	
	//A better version of split that will return remainder as well
	split: function (str, separator, limit) {
		str = str.split(separator);

		if(str.length > limit) {
			var ret = str.splice(0, limit);
			ret.push(str.join(separator));

			return ret;
		}
		return str;		
	},	
}

//Start up pg_pool which is used by Query Streams
const conf = require('./config.js');
var pg_pool = new pg.Pool(conf.conf);

//Begin listening for connections and messages
ctapi_wss.start();
